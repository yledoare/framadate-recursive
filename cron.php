<?php

require_once 'app/inc/init.php';
include("app/inc/config.php");

function cron_log($message)
{
	//echo $message;
	error_log(date('dmY H:i:s') . " - $message", 3, ROOT_DIR . "admin/cron.log");
}

$polls=array();

try{
$pdo = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
} catch (PDOException $e) {
     echo $e->getMessage();
}

$sql_recursive_poll="select id , title from ".TABLENAME_PREFIX."poll where poll_is_recursive = 1";
$sql_old_slot="select id, title, FROM_UNIXTIME(title) as title_datetime, moments from ".TABLENAME_PREFIX."slot where title < (UNIX_TIMESTAMP() - 86400) and poll_id='";

$PollResult = $pdo->query($sql_recursive_poll);
foreach ($PollResult as $poll) {
	$polls[]=$poll['id'];
	cron_log("1 - Poll " . $poll['title'] .", ID : " . $poll['id'] . PHP_EOL);

}

foreach ($polls as $poll) {
	$needshift=FALSE;

	$stmtmin = $pdo->prepare("select min(title) as min from ".TABLENAME_PREFIX."slot where poll_id=?");
	$stmtmin->execute([$poll]);
	$result = $stmtmin->fetch(PDO::FETCH_ASSOC);
	$min = $result['min'];

	$stmtmax = $pdo->prepare("select max(title) as max from ".TABLENAME_PREFIX."slot where poll_id=?");
	$stmtmax->execute([$poll]);
	$result = $stmtmax->fetch(PDO::FETCH_ASSOC);
	$max = $result['max'];

	$diff=($max - $min);
	$floor=floor(($max - $min)/604800);

	$newsql=$sql_old_slot. $poll."'";

	$SlotResult = $pdo->query($newsql);
	foreach ($SlotResult as $slot) {
		cron_log("2 - OLD SLOT ID: ". $slot['id'] . PHP_EOL);
		cron_log("2 - OLD SLOT TITLE :". $slot['title_datetime'] . PHP_EOL);
		cron_log("2 - OLD SLOT Moments : " . $slot['moments'] . PHP_EOL);
		$moments=explode(",",$slot['moments']);
		cron_log("2 - Nb Moments : " . sizeof($moments) . PHP_EOL);
		$slots=sizeof($moments);

		cron_log("2 - For Poll $poll min ($min) date is " . date('m/d/Y h:i:s',$min) . PHP_EOL);
		cron_log("2 - For Poll $poll max ($max) date is " . date('m/d/Y h:i:s',$max) . PHP_EOL);
		cron_log("2 - For Poll $poll calculated weeks is $floor ". PHP_EOL);

		$nextweek=(604800*(1+$floor) + $slot['title']);

                // 2 Weeks
                //$nextweek=1209600  + $slot['title'];
                // 1 Week
                //$nextweek=604800  + $slot['title'];

		$sql="update ".TABLENAME_PREFIX."slot set title=$nextweek where id=".$slot['id'];
		cron_log("3 - Nextweek is " . date('m/d/Y h:i:s',$nextweek) . PHP_EOL);
		cron_log("3 - $sql "  . PHP_EOL);
		$stmt= $pdo->prepare($sql);
		$stmt->execute([$id]);
		$count = $stmt->rowCount();
 		cron_log("4 - $count Slot updated nextweek successfully!". PHP_EOL);
		$data = [
		     'title'=> $nextweek,
     		     'id' => $slot['id']
		];

		$needshift=TRUE;

	}

       if($needshift)
       {
	 cron_log("5 - $Poll $poll needs shift !". PHP_EOL);
	 $data = [ 'poll_id' => $poll ];
         $slots=1 + $slots;
	 $sql="update ".TABLENAME_PREFIX."vote set choices=substring(choices,$slots,100) where poll_id=:poll_id";
	 cron_log("6 - sql : $sql". PHP_EOL);
	 $statement = $pdo->prepare($sql);
	 if($statement->execute($data)) {
		$count = $statement->rowCount();
 			cron_log("7 - $count Vote updated successfully!". PHP_EOL);
	 }

	 $sql="delete from ".TABLENAME_PREFIX."vote where choices = ''";

	 cron_log("8 - sql : $sql". PHP_EOL);
	 $statement = $pdo->prepare($sql);
	 if($statement->execute($data)) {
		$count = $statement->rowCount();
	 		cron_log("9 - $count fd_vote deleted successfully! ". PHP_EOL);
	 }
       }
}
