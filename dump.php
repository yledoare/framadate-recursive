<?php

require_once 'app/inc/init.php';
$dumpfile=ROOT_DIR  . "../dumps/". date('dmY') . ".sql";
include("app/inc/config.php");

$dump = " mysqldump -u" . DB_USER . " -p" . DB_PASSWORD . " -h" . DB_HOST . " " . DB_NAME . " > " . $dumpfile;
echo exec($dump);

if(is_file($dumpfile))
	echo "Success";
else
	echo "$dump Failed";
//echo $dump;
