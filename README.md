# Framadate Recursive

![English](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/20px-Flag_of_the_United_Kingdom.svg.png) Framadate is an online service for planning an appointment or making a decision quickly and easily. No registration is required.

# Installation

```
USERID="$(id -u)" GROUPID="$(id -g)" docker-compose up -d
```

# SQL

```
mysql -u$MYSQL_USER -p$MYSQL_PASSWORD -h$MYSQL_HOST $MYSQL_DB
```

# Logs

```
docker-compose logs --tail=20 -f framadate
```
